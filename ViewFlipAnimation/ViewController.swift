//
//  ViewController.swift
//  ViewFlipAnimation
//
//  Created by S.M.Moinuddin on 17/12/18.
//  Copyright © 2018 S.M.Moinuddin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak private var topV: UIView!
    @IBOutlet weak private var vOne: UIView!
    @IBOutlet weak private var vTwo: UIView!
    @IBOutlet weak private var topImgV: UIImageView!
    @IBOutlet weak private var topLbl: UILabel!
    
    private var shouldHideNichole = false
    private var shouldHideTwo = true

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        hideNichole(shouldHideNichole)
        vTwo.isHidden = true
        
    }
    
    @IBAction private func singleVFlip(_ sender: UIButton) {
        shouldHideNichole = !shouldHideNichole
        UIView.transition(with: topV, duration: 0.7, options: [.transitionFlipFromRight], animations: { [unowned self] in
            self.hideNichole(self.shouldHideNichole)
        })
    }
    
    @IBAction private func flipMainView(_ sender: UIButton) {
        let fromV = shouldHideTwo ? vOne! : vTwo!
        let toV = shouldHideTwo ? vTwo! : vOne!
        shouldHideTwo = !shouldHideTwo
        UIView.transition(from: fromV, to: toV, duration: 0.6, options: [.transitionFlipFromRight, .showHideTransitionViews], completion: nil)
    }
    
    private func hideNichole(_ flag:Bool) {
        let imgName = flag ? "nicoleKidman" : "nKTest"
        topImgV.image = UIImage(named: imgName)
        topLbl.text = flag ? "Nichole" : "Kate"
        topV.backgroundColor = flag ? UIColor.cyan : UIColor.lightGray
    }


}

